# Ansible pull collection for remote administration of debian workstations

This collection is meant for ansible-pull. We need working ansible-pull setup
on the clients with systemd or cronjob.

For now only localhost and host-name works. We can't configure host groups.
