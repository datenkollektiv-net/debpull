#!/bin/bash

# conf
ocrSuff="ocr"
options="-l deu"

ocrmypdf=$(which ocrmypdf)

if ! test -x $(which ocrmypd); then
  echo "Programm ocrmypdf ist nicht installiert"
  echo "bitte mit apt install ocrmypdf installieren"
  sleep 5
  exit 0
fi

# test for arguments
if [ $# -ne 1 ]; then
  echo "Input-PDF-File muss angegeben werden"
  sleep 5
  exit 0
fi

inputFile="$1"
inputDir=$(dirname "$inputFile")
inputFileBase=$(basename -- "$inputFile")
inputExt="${inputFileBase##*.}"
inputBase="$(basename -- "$inputFile" .$inputExt)"

if [ "$inputExt" != "pdf" ] && [ "$inputExt" != "PDF" ]; then
  echo "Datei ist kein PDF-File"
  sleep 5
  exit 0
fi

outputFile=${inputDir}/${inputBase}-${ocrSuff}.${inputExt}

echo $inputFile
$ocrmypdf $options "$inputFile" "$outputFile"

exit 0
