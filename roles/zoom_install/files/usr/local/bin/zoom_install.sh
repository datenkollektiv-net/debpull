#!/bin/bash

package_name="zoom"
package_url="https://zoom.us/client/latest/zoom_amd64.deb"
notify_pre_summary="Zoom-Update"
notify_post_summary="Zoom-Update"
notify_pre_text="Ein Update von Zoom wird installiert.\rBitte warten, bis das Update installiert wurde."
notify_post_text="Das Update von Zoom wurde erfolgreich installiert"

which wget || exit 0
which curl || exit 0

# get installed version
if dpkg -l $package_name > /dev/null 2>&1; then 
        installed_version="$(dpkg -l $package_name | grep $package_name | awk '{print $3}')"
    else
        installed_version=0
fi

echo "Installed $installed_version"

# get latest upstream version
package_location="$(curl -v -I -L $package_url 2>&1|grep -i ^location | cut -d ' ' -f 2)"
available_version="$(echo "$package_location" | sed -e 's|.*prod/||g' -e 's|/.*||g')"
package_filename="zoom_amd64.deb"
echo "${package_filename}"

echo "Available: $available_version"

# compare versions
if [ "$installed_version" == "$available_version" ]; then
	echo "Latest version of $package_name already installed."
	exit 0
    else
        # install if differs
	# try to notify
        which notify-send-all-loop && notify-send-all-loop -i system-software-update "$notify_pre_summary" "$notify_pre_text" &
        tmp_dir=$(mktemp -d)
	chown _apt "$tmp_dir"
	echo $package_url
        wget "${package_url}" -O "${tmp_dir}/${package_filename}"
	apt-get -y install "$tmp_dir/$package_filename"
	rm "$tmp_dir/$package_filename"
	# try to notify
        which notify-send-all-loop && notify-send-all-loop -i system-software-update "$notify_post_summary" "$notify_post_text" &
fi

exit 0
