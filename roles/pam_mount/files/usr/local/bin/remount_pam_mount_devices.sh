#!/bin/bash

# Script testet, ob beim Login alle Laufwerke (außer /home) mit pam_mount
# eingebunden wurden. Wenn nicht, wird nochmal ein Login-Prozess durchgeführt
# Dies funktioniert nur, wenn pam_mount so konfiguriert wird, dass es bei 
# jedem Login - und nicht nur beim ersten Login - ausgeführt wird.

pam_mount_config=/etc/security/pam_mount.conf.xml
myuid=$(whoami)

test -r $pam_mount_config || exit 0

test_pam_mount() {
  not_mounted=$(for i in $(cat $pam_mount_config |grep mountpoint=| sed  's/.*mountpoint="//g' | grep -E -v '/home/%\(USER\)/?\"' | sed -e 's/%(USER)/'$myuid'/g' | cut -d '"' -f 1); do mount |grep -q $i|| echo $i; done)
  if [ "$not_mounted" == "" ]; then
      # everyting is mounted
      zenity --width=300 --info --text "Alle Netzwerk-Ordner scheinen eingebunden zu sein."
      exit 0
  fi
  echo 1
}

ask_remount() {
zenity --width=500 --question --text "Das Einbinden der Netzwerk-Ordner beim Login ist fehlgeschlagen.

Folgende Ordner sind nicht eingebunden:

  $not_mounted

Vielleicht bestand beim Login noch keine Netzwerkverbindung zum Server. Wenn Du die Netzerk-Ordner jetzt einbinden willst, vergewissere dich, dass die notwendige Netzwerkverbindung existiert (LAN/WLAN/VPN) und gibt anschließend zum Einbinden dein Nutzerpasswort ein. Wenn Du keine Netzwerk-Ordner einbinden willst, kannst du abbrechen.

Wenn du jetzt abbrichst, kannst Du diesen Dialog mit \"Netzwerk-Ordner Einbinden\" jederzeit aus dem Menü unter System(werkzeuge) wieder ausführen."
}

read_password() {
  password=$(zenity --password)
}

force_login() {
    echo $password | su $USER
}

test_home_dir_via_pam_mount() {
if grep mountpoint= $pam_mount_config | grep -E  '/home/%\(USER\)/?\"'; then

      zenity --width=300 --info --text "Dein Home-Ordner wird beim Anmelden automatisch eingebunden. In diesem Fall können die automatisch eingebundenen Laufwerke nicht erneut eingebunden werden. Bitte melde dich in diesem Fall ab und erneut an."

exit 0

fi

}

test_pam_mount

if ask_remount; then
  while [ "$login_successfull" != "1" ]; do
      # test for remote home
      test_home_dir_via_pam_mount
      read_password || exit 0
      if force_login; then
        zenity --width=300 --info --text "Login erfolgreich. Netzwerk-Ordner sollten jetzt eingebunden werden, wenn eine Netzwerkverbindung besteht."
        login_successfull=1
        # test again:
        test_pam_mount
        if [ "not_mounted" != "" ]; then
          zenity --width=300 --info --text "Nicht alle Netzwerk-Ordner konnten eingebunden werden. Vielleicht hast du nicht für alle Netzwerk-Ordner eine Zugangsberechtigung oder es gibt nach wie vor keine Verbindung zum Server. Prüfe deine Netzwerkverbindung oder wende dich an deine:n Administrator:in. Folgende Ordner konnten nicht eingebunden werden:
          $not_mounted"
        fi
      else
        zenity --info --text "Login war nicht erfolgreich. Bitte nochmal versuchen"
      fi
  done
else
  exit 0
fi

exit 0
